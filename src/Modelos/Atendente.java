/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

import Interface.Principal;
import javax.swing.ImageIcon;
/**
 *
 * @author Guia
 */
public class Atendente extends Thread{
    
    private String nomeAtendente;
    private javax.swing.JLabel atendenteLabel;
    private Torcedor torcedorSendoAtendido;
    private long tempo;
    private int atendenteIndex;
    private boolean filaVazia = false;
    
    public Atendente(String nomeAtendente, int atendenteIndex){
        this.nomeAtendente = nomeAtendente;
        this.atendenteLabel = Principal.atendentesLabels.get(atendenteIndex);
        this.atendenteLabel.setVisible(true);
        this.atendenteIndex = atendenteIndex;
    }
    
    @Override
    public void run(){
        while(true){
            filaVazia = false;
            
            System.out.println("Caixa "+ nomeAtendente +" livre...");
            
            //espera um torcedor aparecer na fila
            try {
            Bilheteria.torcedoresSemaphore.acquire();
            } catch (InterruptedException e) {
            }
            
            //solicita exclusividade no processamento
            try {
            Bilheteria.mutex.acquire();
            } catch (InterruptedException e) {
            }
            /*-----------------------------REGIÃO CRÍTICA----------------------------------*/

            //o acesso à lista de torcedores e de atendentes é compartilhado, por isso é crítico
            if(!(Bilheteria.torcedores.isEmpty())){
                int index = 0;
                torcedorSendoAtendido = Bilheteria.torcedores.get(index);
                tempo =  torcedorSendoAtendido.getTempoDeAtendimento();
                while (torcedorSendoAtendido.getSendoAtendido() && index < Bilheteria.torcedores.size()){
                    if (index == (Bilheteria.torcedores.size()-1)){
                        filaVazia = true;
                    }
                    torcedorSendoAtendido = Bilheteria.torcedores.get(index);
                    index++;
                }
                if (!filaVazia) {
                    torcedorSendoAtendido = Bilheteria.torcedores.get(index);
                    Bilheteria.torcedores.remove(0); //REMOVER DO ARRAYLIST
                    Principal.arrayParaQuantidadeDeClientes.get(0).setText("Fila: "+Bilheteria.torcedores.size());
                    torcedorSendoAtendido.setatendenteIndex(this.atendenteIndex);
                    torcedorSendoAtendido.setSendoAtendido(true);
                    System.out.println("Caixa "+ nomeAtendente +" atendendo "+" torcedor "+torcedorSendoAtendido.getNomeDoTorcedor());
                    System.out.println("Cliente  "+torcedorSendoAtendido.getNomeDoTorcedor()+" sendo atendido pelo caixa "+nomeAtendente);
                    
                }
            }

             /*-----------------------------FIM REGIÃO CRÍTICA----------------------------------*/

            //a exclusividade é terminada
            Bilheteria.mutex.release();


            //atendendo(Animação)
            if (!filaVazia) {
                atendendo();
            }
            //o caixa é liberado para atender outro torcedor
            Bilheteria.caixasSemaphore.release();
        }
    }
    
    public void atendendo(){
        
        long start = System.currentTimeMillis();
        String path;
        int indiceAtendente = 0;
        //temporização do tipo CPU Boud
        while(tempo*1000 > (System.currentTimeMillis() - start)){
            if (indiceAtendente == 0){
                path = "/Imagens/AtendenteEmExecucao/AtendenteEmEspera.png";
                indiceAtendente = 1;
            }else{
                path = "/Imagens/AtendenteEmExecucao/AtendenteExecutando.png";
                indiceAtendente = 0;
            }
            
            
            Principal.atendentesLabels.get(atendenteIndex).setIcon(new ImageIcon(getClass().getResource(path)));
        }   
        Principal.atendentesLabels.get(atendenteIndex).setIcon(new ImageIcon(getClass().getResource("/Imagens/AtendenteEmExecucao/AtendenteEmEspera.png")));
       
    }
    
}

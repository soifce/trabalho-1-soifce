/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

import Interface.Principal;
import static java.lang.Math.abs;
import javax.swing.ImageIcon;

/**
 *
 * @author Guia
 */
public class Torcedor extends Thread{
    
    private String nomeTorcedor;
    private int atendenteIndex;
    private long  tempoDeAtendimento;
    private boolean sendoAtendido = false;
    
    public Torcedor(long tempoDeAtendimento, int senha){
        this.tempoDeAtendimento = tempoDeAtendimento;
        nomeTorcedor = "Torcedor " + senha;
    }
    
    public long getTempoDeAtendimento(){
        return tempoDeAtendimento;
    }
    
    public void setatendenteIndex(int atendenteIndex){
        this.atendenteIndex = atendenteIndex;
    }
    
    public void setSendoAtendido(boolean sendoAtendido){
        this.sendoAtendido = sendoAtendido;
    }
    
    public boolean getSendoAtendido(){
        return sendoAtendido;
    }
    
    @Override
    public void run(){
       System.out.println("\n"+nomeTorcedor +" aguardando na fila...");
        
       //exclusividade solicitada ao processador (entre as threads)
        try {
            Bilheteria.mutex.acquire();
        } catch (InterruptedException e) {
        }
        /*-----------------------------REGIÃO CRÍTICA----------------------------------*/
//        System.out.println("REGIÃO CRITICA DO TORCEDOR "+ nomeTorcedor);
        Principal.torcedoresLabels.get(atendenteIndex).setVisible(false);
        /*-----------------------------FIM REGIÃO CRÍTICA----------------------------------*/
        
        //o torcedor libera o semáforo para que um atendente o atenda
        Bilheteria.torcedoresSemaphore.release();
        //a exclusivvidade é terminada
        Bilheteria.mutex.release();
        
        //o torcedor aguarda um atendente e então o acorda sua thread
        try {
            Bilheteria.caixasSemaphore.acquire();
        } catch (InterruptedException e) {
        }
        //o torcedor aparece em movimento de atendimento
        animarCliente();
    }
    
    public String getNomeDoTorcedor(){
        return nomeTorcedor;
    }
    
    private void animarCliente(){
        String path;
        int indiceTorcedor = 1;
        long segundos = tempoDeAtendimento*1000;
        long start = System.currentTimeMillis();
        
        System.out.println(nomeTorcedor +" animando");
        
        while(segundos > System.currentTimeMillis() - start){
            if ((System.currentTimeMillis() - start) < 50){ //GO HORSE SATÂNICA
                Principal.torcedoresLabels.get(0).setVisible(false);
                Principal.nomeDosTorcedoresLabels.get(0).setVisible(false);  
            }
            if (indiceTorcedor < 10){
                path = "/Imagens/TorcedorEmExecucao/frame-0"+indiceTorcedor+".png";

            }else{
                path = "/Imagens/TorcedorEmExecucao/frame-"+indiceTorcedor+".png";
            }
            int tempoRestante = (int)(start + segundos - System.currentTimeMillis())/1000;
            Principal.nomeDosTorcedoresLabels.get(atendenteIndex).setText(nomeTorcedor +": "+ abs(tempoRestante));
            Principal.torcedoresLabels.get(atendenteIndex).setIcon(new ImageIcon(getClass().getResource(path)));
            Principal.torcedoresLabels.get(atendenteIndex).setVisible(true);
            Principal.nomeDosTorcedoresLabels.get(atendenteIndex).setVisible(true);
            
            indiceTorcedor++;
            if (indiceTorcedor==19){
                indiceTorcedor = 1;
            }
        }
        tempoDeAtendimento = 0;
        System.out.println(nomeTorcedor +" foi atendido");
        Principal.torcedoresLabels.get(atendenteIndex).setVisible(false);
        Principal.nomeDosTorcedoresLabels.get(atendenteIndex).setVisible(false);
    }
}

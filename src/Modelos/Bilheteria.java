/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

import java.util.ArrayList;
import java.util.concurrent.Semaphore;
/**
 *
 * @author Guia
 */

public class Bilheteria extends Thread{
    public static Semaphore mutex;
    public static Semaphore torcedoresSemaphore;
    public static Semaphore caixasSemaphore;

    public static ArrayList<Atendente> atendentes;
    public static ArrayList<Torcedor> torcedores;
    
    private static int tamanhoDaFilaClientes = 0;
    private int torcedoresAtendidos = 0;
    private final int quantidadeDeAtendentes;
    
    public Bilheteria(int quantidadeDeAtendentes){
        mutex = new Semaphore(1);
        caixasSemaphore = new Semaphore(quantidadeDeAtendentes);
        torcedoresSemaphore = new Semaphore(0, true);
        
        atendentes = new ArrayList<>();
        torcedores = new ArrayList<>();
        
        for (int index=0; index<quantidadeDeAtendentes; index++){
            String nomeAtendente = "Atendente "+index;
            Atendente atendente = new Atendente(nomeAtendente, index);
            atendentes.add(atendente);
            atendente.start();
        }
        
        this.quantidadeDeAtendentes = quantidadeDeAtendentes;
        
        
    }
    
    public void addTorcedor(int tempoDeAtendimento){
        Torcedor torcedor = new Torcedor(tempoDeAtendimento, torcedoresAtendidos);
        torcedores.add(torcedor);
        torcedoresAtendidos++;
        torcedor.start();
    }
    
    public int getTamanhoDaFila(){
        return tamanhoDaFilaClientes;
    }
    
    public int getTorcedoresAtendidos(){
        return torcedoresAtendidos;
    }

    @Override
    public void run() {
        super.start(); //To change body of generated methods, choose Tools | Templates.
        
    }
    
    
}
